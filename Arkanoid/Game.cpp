#include "pch.h"
#include "Game.h"
#include "GameObject.h"
#include "GameController.h"
#include "Collider.h"
extern void ExitGame() noexcept;

using namespace DirectX;
using namespace DirectX::SimpleMath;

using Microsoft::WRL::ComPtr;

Game::Game() noexcept(false)
{
	m_deviceResources = std::make_unique<DX::DeviceResources>();
	m_deviceResources->RegisterDeviceNotify(this);
}

void Game::Initialize(HWND window, int width, int height)
{
	m_deviceResources->SetWindow(window, width, height);

	m_deviceResources->CreateDeviceResources();
	CreateDeviceDependentResources();

	m_deviceResources->CreateWindowSizeDependentResources();
	CreateWindowSizeDependentResources();

	m_keyboard = std::make_unique<Keyboard>();
	m_mouse = std::make_unique<Mouse>();
	m_mouse->SetWindow(window);
	AUDIO_ENGINE_FLAGS eflags = AudioEngine_Default;
#ifdef _DEBUG
	eflags |= AudioEngine_Debug;
#endif
	m_audEngine = std::make_shared<AudioEngine>(eflags);
	GameController::GetInstance().setCenter(m_center);
	GameController::GetInstance().restartGame();
}

void Game::Tick()
{
	m_timer.Tick([&]()
		{
			Update(m_timer);
		});

	Render();
}

void Game::Update(DX::StepTimer const& timer)
{
	float elapsedTime = float(timer.GetElapsedSeconds());

	auto kb = m_keyboard->GetState();
	m_keys.Update(kb);

	if (kb.Escape)
	{
		ExitGame();
	}
	if (kb.R) {
		GameController::GetInstance().restartGame();
	}
	if (GameController::GetInstance().getNumOfBricks() > 0 && GameController::GetInstance().getNumOfLifes() > 0) {
		for (GameObject* gameObject : GameController::GetInstance().getObjects()) {
			gameObject->Update(elapsedTime, kb);
		}
	}
}

void Game::Render()
{
	if (m_timer.GetFrameCount() == 0)
	{
		return;
	}

	Clear();

	m_deviceResources->PIXBeginEvent(L"Render");
	auto context = m_deviceResources->GetD3DDeviceContext();
	context->OMSetBlendState(m_states->Opaque(), nullptr, 0xFFFFFFFF);
	context->OMSetDepthStencilState(m_states->DepthNone(), 0);
	context->RSSetState(m_states->CullNone());

	m_effect->Apply(context);

	context->IASetInputLayout(m_inputLayout.Get());

	m_batch->Begin();

	for (const GameObject* gameObject : GameController::GetInstance().getObjects()) {
		gameObject->Draw(m_batch.get());
	}
	for (const Collider* collider : GameController::GetInstance().getGameColliders(GameController::CollisionTag::wall)) {
		collider->Draw(m_batch.get());
	}

	m_batch->End();

	m_spriteBatch->Begin();
	std::string tmp = std::to_string(GameController::GetInstance().getNumOfBricks()).c_str();
	
	if (GameController::GetInstance().getNumOfBricks() == 0) {
		PostQuitMessage(0);
	}
	if (GameController::GetInstance().getNumOfLifes() == 0) {
		PostQuitMessage(0);
	}
	m_spriteBatch->End();

	m_deviceResources->PIXEndEvent();
	m_deviceResources->Present();

	m_graphicsMemory->Commit();
}

void Game::Clear()
{
	m_deviceResources->PIXBeginEvent(L"Clear");

	auto context = m_deviceResources->GetD3DDeviceContext();
	auto renderTarget = m_deviceResources->GetRenderTargetView();
	auto depthStencil = m_deviceResources->GetDepthStencilView();

	context->ClearRenderTargetView(renderTarget, Colors::Black);
	context->ClearDepthStencilView(depthStencil, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	context->OMSetRenderTargets(1, &renderTarget, depthStencil);

	auto viewport = m_deviceResources->GetScreenViewport();
	context->RSSetViewports(1, &viewport);

	m_deviceResources->PIXEndEvent();
}

void Game::OnActivated()
{
	m_keys.Reset();
	m_mouseButtons.Reset();
}

void Game::OnDeactivated()
{
}

void Game::OnSuspending()
{
	m_audEngine->Suspend();
}

void Game::OnResuming()
{
	m_timer.ResetElapsedTime();
	m_keys.Reset();
	m_mouseButtons.Reset();
	m_audEngine->Resume();
}

void Game::OnWindowMoved()
{
	auto r = m_deviceResources->GetOutputSize();
	m_deviceResources->WindowSizeChanged(r.right, r.bottom);
}

void Game::OnWindowSizeChanged(int width, int height)
{
	if (!m_deviceResources->WindowSizeChanged(width, height))
		return;

	CreateWindowSizeDependentResources();

}

void Game::GetDefaultSize(int& width, int& height) const noexcept
{
	width = 800;
	height = 600;
}

void Game::CreateDeviceDependentResources()
{
	auto device = m_deviceResources->GetD3DDevice();
	m_states = std::make_unique<CommonStates>(device);
	m_graphicsMemory = std::make_unique<GraphicsMemory>(device);
	auto context = m_deviceResources->GetD3DDeviceContext();
	m_spriteBatch = std::make_unique<SpriteBatch>(context);
	m_effect = std::make_unique<BasicEffect>(device);
	m_batch = std::make_unique<PrimitiveBatch<DirectX::VertexPositionColor>>(context);
	m_effect->SetVertexColorEnabled(true);

	DX::ThrowIfFailed(
		CreateInputLayoutFromEffect<DirectX::VertexPositionColor>(device, m_effect.get(),
			m_inputLayout.ReleaseAndGetAddressOf())
	);
}

void Game::CreateWindowSizeDependentResources()
{
	auto size = m_deviceResources->GetOutputSize();
	m_center.x = float(size.right) / 2.0f;
	m_center.y = float(size.bottom) / 2.0f;
	m_fontPos.x = m_center.x;
	m_fontPos.y = m_center.y;
	Matrix proj = Matrix::CreateScale(2.f / float(size.right),
		-2.f / float(size.bottom), 1.f)
		* Matrix::CreateTranslation(-1.f, 1.f, 0.f);

	m_effect->SetProjection(proj);

}

void Game::OnDeviceLost()
{
	m_graphicsMemory.reset();
	m_states.reset();
	m_effect.reset();
	m_batch.reset();
	m_font.reset();
	m_spriteBatch.reset();
	m_inputLayout.Reset();
}

void Game::OnDeviceRestored()
{
	CreateDeviceDependentResources();

	CreateWindowSizeDependentResources();
}
