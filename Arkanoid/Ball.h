#pragma once
#include "GameObject.h"
class Collider;

class Ball :public GameObject {
private:
	float m_speed = 300.0f;
	bool m_isXSwitch = false;
	bool m_isYSwitch = false;
	std::unique_ptr<DirectX::SoundEffect> _ambient;
public:
	Ball(Vector2 position);
	virtual void Update(float i_deltaTime, const KeyboardState& i_kb) override;
	virtual void Draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const override;
	virtual void OnCollisionStart(Collider* obj1, Collider* obj2) override;
};