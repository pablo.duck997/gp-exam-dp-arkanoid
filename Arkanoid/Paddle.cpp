#include "pch.h"
#include "Paddle.h"
#include "GameController.h"
#include "Collider.h"

Paddle::Paddle(Vector2 position) :GameObject(position, Vector2(100.0f, 25.0f), DirectX::Colors::BlueViolet)
{
	_colliders.push_back(new Collider(this, GameController::CollisionTag::paddle, Vector2(0.0f, 0.0f), _size));
}

void Paddle::Update(float i_deltaTime, const KeyboardState& keyboard)
{
	GameObject::Update(i_deltaTime, keyboard);
	Vector2 movementVelocity = Vector2::Zero;

	if (keyboard.Left || keyboard.A) {
		movementVelocity.x -= 1.f;
	}
	if (keyboard.Right || keyboard.D) {
		movementVelocity.x += 1.f;
	}
	movementVelocity *= speedPaddle;

	setVelocity(movementVelocity);
}

void Paddle::Draw(DirectX::PrimitiveBatch<DirectX::VertexPositionColor>* batch) const
{
	GameObject::Draw(batch);
}

void Paddle::OnCollisionStart(Collider* obj1, Collider* obj2)
{
	if (obj2->getCollisionTag() == GameController::CollisionTag::wall) {	
		DirectX::SimpleMath::Rectangle wallRect = obj2->getWorldRectangle();
		if (_position.x > wallRect.Center().x) {
			setPosition(Vector2(wallRect.x + wallRect.width + _size.x / 2, _position.y));
		}
		else {
			setPosition(Vector2(wallRect.x - _size.x / 2, _position.y));
		}
	}
}

void Paddle::OnCollisionStay(Collider* obj1, Collider* obj2)
{
	if (obj2->getCollisionTag() == GameController::CollisionTag::wall) {
		DirectX::SimpleMath::Rectangle wallRect = obj2->getWorldRectangle();
		if (_position.x > wallRect.Center().x) {
			setPosition(Vector2(wallRect.x + wallRect.width + _size.x, _position.y));
		}
		else {
			setPosition(Vector2(wallRect.x - _size.x, _position.y));
		}
	}
}

